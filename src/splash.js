import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image
} from 'react-native';
import logo from '../images/logo.png';

class Splash extends Component {
  componentWillMount()
  {
      setTimeout(()=>{this.props.navigation.navigate('main')},800);
  }
  render() {
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
          backgroundColor="#4F6D7A"
        />
        <Image source={logo} style={styles.imageStyle}>
        </Image>
        <Text style={styles.welcome}>
          Welcome to MelTech Store!
        </Text>
      </View>
    );
  }
}

export default Splash;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4F6D7A',
  },
  welcome: {
    fontSize: 23,
    textAlign: 'center',
    margin: 30,
    color: '#F5FCFF',
  },
  imageStyle: {
    width: 200,
    height: 200,
    margin: 20,
  },
});