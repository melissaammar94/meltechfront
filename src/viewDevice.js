import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  TouchableOpacity
} from 'react-native';
import edit from '../images/edit.png';
import deleteImg from '../images/delete.png';

class ViewDevice extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('itemName', '') + " Details",
      headerStyle: {backgroundColor:'#4F6D7A'}, 
      headerTintColor: '#FFFFFF'
    };
  };
  
  render() {
    const { navigation } = this.props;
    const item = navigation.getParam('item', 'NO-ITEM');
    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="light-content"
          backgroundColor="#4F6D7A"
        />
        <View style={styles.containerList}/>
          <View style={styles.containerView}>
            <Text style={styles.item}>
                Device Name: {item.name} {"\n"}
                Category: {item.category} {"\n"}
                Brand: {item.brand} {"\n"}
                Price: {item.price} {"\n"}
                Quantity: {item.quantity} {"\n"}
                Storage: {item.specs.storage} {"\n"}
                Model Number: {item.specs.modelNumber} {"\n"}
                Processor: {item.specs.processor} {"\n"}
                Operating System: {item.specs.operatingSystem} {"\n"}
                RAM: {item.specs.ram} {"\n"}
            </Text>
          </View>
        <TouchableOpacity style={styles.deleteButton} onPress={() => 
            navigation.navigate('process',{id: item.id, action: 'Delete'})}>
        <Image source={deleteImg} style={styles.imageStyle}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.editButton} onPress={() => navigation.navigate('editDevice',
        {item: item, name: item.name})}>
        <Image source={edit} style={styles.imageStyle}/>
        </TouchableOpacity>
      </View>
    );
  }
}

export default ViewDevice;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4F6D7A',
  },
  containerList: {
    margin: 1,
    alignSelf: 'stretch',
    backgroundColor: '#4F6D7A',
  },
  containerView: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#F5F5DC',
  },
  item: {
    position: 'absolute',
    top: 20,
    left: 20,
    fontSize: 22,
    textAlign: 'left',
    flex:1
  },
  deleteButton: {
    position: 'absolute',
    zIndex: 11,
    right: 20,
    bottom: 25,
    backgroundColor: '#4F6D7A',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  editButton: {
    position: 'absolute',
    zIndex: 11,
    left: 20,
    bottom: 25,
    backgroundColor: '#4F6D7A',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  imageStyle: {
    width: 50,
    height: 50,
  },
});