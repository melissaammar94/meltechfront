import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native';
import done from '../images/done.png';

class Process extends Component {

  constructor(props) {
    super(props);
    this.state ={ isLoading: true}
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: null,
      title: navigation.getParam('action', '') + " Device",
      headerStyle: {backgroundColor:'#4F6D7A'}, 
      headerTintColor: '#FFFFFF'
    };
  }

  //fetching the data from the backend APIs
  componentDidMount(){

    const action = this.props.navigation.getParam('action', 'NO-ITEM');
    if (action == 'Delete')
    {
      const id = this.props.navigation.getParam('id', '0');
      return fetch('https://meltech.herokuapp.com/devices/' + id,{method: 'DELETE'})
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            isLoading: false,
            dataSource: responseJson,
          }, function(){

          });
        })
        .catch((error) =>{
          console.error(error);
        });
      }
      else if (action == 'Edit'){
        const id = this.props.navigation.getParam('id', '0');
        const body = this.props.navigation.getParam('body', '{"":""}');
        return fetch('https://meltech.herokuapp.com/devices/' + id,
        {method: 'PUT', headers: {'Content-Type' : 'application/json'},
          body: JSON.stringify({body})
        }).then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            isLoading: false,
            dataSource: responseJson,
          }, function(){});
        })
        .catch((error) =>{
          console.error(error);
        });
      }
      else if (action == 'Add')
      {
        const body = this.props.navigation.getParam('body', '{"":""}');
        return fetch('https://meltech.herokuapp.com/devices',{method: 'POST',
          headers: {'Content-Type' : 'application/json'},
          body: JSON.stringify({body})})
        .then((response) => response.json())
        .then((responseJson) => {
          this.setState({
            isLoading: false,
            dataSource: responseJson,
          }, function(){

          });
        })
        .catch((error) =>{
          console.error(error);
        });
      }
  }

  render(){
    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <StatusBar
            barStyle="light-content"
            backgroundColor="#4F6D7A"
          />
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      
      <View style={styles.container}> 
        <StatusBar
          barStyle="light-content"
          backgroundColor="#4F6D7A"
        />
        <Text style={styles.welcome}>
            {this.state.dataSource.message}
        </Text>
        <TouchableOpacity style={styles.doneButton} onPress={() => this.props.navigation.navigate('main')}>
        <Image source={done} style={styles.imageStyle}/>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Process;
const styles = StyleSheet.create({
  welcome: {
    fontSize: 23,
    textAlign: 'center',
    margin: 30,
    color: '#000000',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5F5DC',
  },
  doneButton: {
    position: 'absolute',
    zIndex: 11,
    right: 25,
    bottom: 35,
    backgroundColor: '#4F6D7A',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  imageStyle: {
    width: 50,
    height: 50,
  },
});