import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ActivityIndicator,
  FlatList,
  TextInput,
  TouchableOpacity
} from 'react-native';

class Main extends Component {

  constructor(props) {
    super(props);
    this.state ={ isLoading: true}
  }

  static navigationOptions= { 
      headerLeft: null, 
      title: 'MelTech -   Tap To View Device', 
      // header: (
      //   <TextInput style={styles.textInput}
      //     placeholder='Search for device'
      //     placeholderTextColor='#FFFFFF'
      //     underlineColorAndroid='transparent'
      //   />
      // ), 
      headerStyle: {backgroundColor:'#4F6D7A'}, 
      headerTintColor: '#FFFFFF'
  }

  //fetching the data from the backend APIs
  componentDidMount(){
    return fetch('https://meltech.herokuapp.com/devices/category/Computer')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson.data,
        }, function(){

        });
      })
      .catch((error) =>{
        console.error(error);
      });
  }

  _keyExtractor = (item, index) => item.name;

  render(){

    if(this.state.isLoading){
      return(
        <View style={{flex: 1, padding: 20}}>
          <StatusBar
            barStyle="light-content"
            backgroundColor="#4F6D7A"
          />
          <ActivityIndicator/>
        </View>
      )
    }

    return(
      <View style={styles.container}> 
        <StatusBar
          barStyle="light-content"
          backgroundColor="#4F6D7A"
        />
        <FlatList style={styles.containerList}
          data={this.state.dataSource}
          renderItem={({item}) => 
          <TouchableOpacity style={styles.containerView} index={item.name}
          onPress={() => this.props.navigation.navigate('viewDevice',{ itemName: item.name, item: item })}>
            <Text style={styles.item}> {item.name}</Text>
            <Text style={styles.item}> {item.category}</Text>
            <Text style={styles.item}> {item.price} {item.currency} </Text>
            </TouchableOpacity>}
          keyExtractor={this._keyExtractor}
        />
        <TouchableOpacity style={styles.addButton} onPress={() => this.props.navigation.navigate('addDevice')}>
          <Text style={styles.addButtonText}>+</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Main;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#4F6D7A',
  },
  containerList: {
    alignSelf: 'stretch',
    backgroundColor: '#4F6D7A',
  },
  containerView: {
    margin: 10,
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    backgroundColor: '#F5F5DC',
  },
  item: {
    margin: 1,
    fontSize: 23,
    textAlign: 'center',
    flex:1
  },
  textInput: {
    alignSelf: 'stretch',
    color: '#FFFFFF',
    padding: 20,
    backgroundColor: '#4F6D7A',
    borderTopWidth: 2,
    borderTopColor: '#ededed'
  },
  addButton: {
    position: 'absolute',
    zIndex: 11,
    right: 20,
    bottom: 25,
    backgroundColor: '#4F6D7A',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  addButtonText: {
    color: '#FFFFFF',
    fontSize: 35,
  },
});