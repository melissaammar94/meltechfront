import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  TouchableOpacity
} from 'react-native';
import sad from '../images/sad-face.png';
import done from '../images/done.png';

class EditDevice extends Component {

  constructor(props) {
    super(props);
    this.state ={ isLoading: true}
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: null,
      title: "Edit Device " + navigation.getParam('name', ''),
      headerStyle: {backgroundColor:'#4F6D7A'}, 
      headerTintColor: '#FFFFFF'
    };
  }

  render(){
    return(
      
      <View style={styles.container}> 
        <StatusBar
          barStyle="light-content"
          backgroundColor="#4F6D7A"
        />
        <Text style={styles.welcome}>
            Oops! Looks like someone couldn't develop this page!
        </Text>
        <Image source={sad} style={styles.sadStyle}/>
        <TouchableOpacity style={styles.addButton} onPress={() => this.props.navigation.navigate('main')}>
        <Image source={done} style={styles.imageStyle}/>
        </TouchableOpacity>
      </View>
    );
  }
}

export default EditDevice;
const styles = StyleSheet.create({
  welcome: {
    fontSize: 23,
    textAlign: 'center',
    margin: 30,
    color: '#000000',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5F5DC',
  },
  addButton: {
    position: 'absolute',
    zIndex: 11,
    right: 25,
    bottom: 35,
    backgroundColor: '#4F6D7A',
    width: 90,
    height: 90,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 8,
  },
  imageStyle: {
    width: 50,
    height: 50,
  },
  sadStyle: {
    width: 60,
    height: 60,
  },
});