import { Component } from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import Splash from "./src/splash.js";
import Main from "./src/main.js";
import AddDevice from "./src/addDevice.js";
import ViewDevice from "./src/viewDevice.js";
import EditDevice from "./src/editDevice.js";
import Process from "./src/process.js"

class App extends Component {
  render() {
  }
}

const Navigation= createStackNavigator(
  {
    Splash: { screen: Splash, navigationOptions: { header: null } },
    main: { screen: Main },
    addDevice: { screen: AddDevice },
    viewDevice: { screen: ViewDevice },
    editDevice: { screen: EditDevice },
    process: { screen: Process },
  },
  {
    initialRouteName: 'Splash'
  }
)

export default createAppContainer(Navigation);